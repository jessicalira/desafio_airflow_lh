# Desafio Airflow Resultados #


## Objetivo

Esse desafio foi criado para colocarmos em prática os conceitos de Orquestração com Airflow.


### Passos que foram executaods ###

Primeiramente, duas funções foram criadas para cada task (task1 e task2, respectivamente). Essas funções estão disponíveis em detalhes abaixo.

A task1 teve como objetivo a leitura da tabela “Order” de um banco disponível para o desafio e escrever um arquivo nomeado 	**“output_orders.csv”**. A task2 teve como objetivo a leitura da tabela “OrderDetail” do banco disponível e fazer um JOIN com o “output_orders.csv” para uma condição especificada: COUNT(Quantity) e WHERE = 'Rio de Janeiro'. Como resultado da task2, tem-se a escrita de um arquivo nomeado 	**“count.txt”	**.

Em seguida, a dag das tasks e sua dependência foram criadas. Esse arquivo da dag com as funções “def task1():”, “def task2():” e a dag propriamente dita foi nomeado como “example_desafio.py”. Esse arquivo, por sua vez, foi importado para o Airflow onde foi possível rodar a DAG sem erros e gerar o arquivo final nomeado 	**“final_output.txt”	**.



## Construindo a DAG

## Função da Task 1

Aqui estarei explicando como foi feito a função da task1. 

- 1)	Inicialmente, foi criado uma conexão SQL com nosso banco de dados SQLite e um cursor;
- 2)	Depois foi feito a query referente a task 1 (leitura da tabela “Order”)
- 3)	Em seguida, foi criado um arquivo chamado "output_orders.csv"
- 4)	Por fim, foi feito fechado a conexão SQL



```
def task1():
    ## Create a SQL connection to our SQLite database and a cursor
    conn = sqlite3.connect("data/Northwind_small.sqlite")
    ## Create a cursor
    cur = conn.cursor()

    ## Query the databse
    data1 = cur.execute('SELECT * FROM "Order"')

    ## Write a file called "output_orders.csv"
    with open("output_orders.csv", "w") as f:
        for row in data1.fetchall():
            csv.writer(f).writerow(row)

    ## Commit and colose connection 
    conn.commit()
    conn.close()
    return 
```

## Função da Task 2 

Aqui estarei explicando como foi feito a função da task2. 

- 1)	Inicialmente, foi criado uma conexão SQL com nosso banco de dados SQLite e um cursor;
Para fazer o JOIN entre o banco e o arquivo .csv, tive que criar uma nova tabela no banco com o conteúdo do arquivo “output_orders.csv” disponível no meu banco. Para isso fiz os seguintes passos:
- 2)	Descartar tabelas com mesmo nome “output_orders_csv”
- 3)	Criar uma tabela com o nome “output_orders_csv” 
- 5)	Ler o arquivo "output_orders.csv"
- 4)	Inserir os valores do arquivo .csv na tabela criada com o nome “output_orders_csv” no banco
- 5)	Executar a query específica para a task 2 para contagem da coluna Quantity. Para isso, foi feito um JOIN na tabela “OrderDetails” com a nova tabela “output_orders_csv” para a igualdade dos Ids correspondentes e para o ShiCity = Rio de Janeiro. 
- 6)	Em seguida, foi criado um arquivo chamado "count.txt"
- 7)	Por fim, foi feito fechado a conexão SQL



```
def task2():
    ## Import "output_orders.csv"
    ## Create a SQL connection to our SQLite database
    conn = sqlite3.connect("data/Northwind_small.sqlite")

    ## Create a cursor
    cur = conn.cursor()

    ## Drop table with same name
    cur.execute("Drop table if exists output_orders_csv;")
    
    ## Create table 
    cur.execute("""CREATE TABLE output_orders_csv (
        Id INTEGER, 
        CustomerId VARCHAR, 
        EmployeeId INTEGER,         
        OrderDate VARCHAR, 
        RequiredDate VARCHAR, 
        ShippedDate VARCHAR, 
        ShipVia INTEGER, 
        Freight DECIMAL, 
        ShipName VARCHAR, 
        ShipAddress VARCHAR, 
        ShipCity VARCHAR, 
        ShipRegion VARCHAR, 
        ShipPostalCode VARCHAR, 
        ShipCountry VARCHAR,
        PRIMARY KEY (Id)
        )""")
    
    ## Open "output_orders.csv" file
    with open("output_orders.csv",'r') as csvfile:
        csvfile = csv.reader(csvfile, delimiter=',')
        all_values = []
        for row in csvfile:
            value = (row[0], row[1],row[2],row[3], row[4],row[5],row[6], row[7],row[8], row[9],row[10],row[11], row[12],row[13])
            all_values.append(value)
    
    ## Insert values to table 
    query= """INSERT INTO output_orders_csv (Id, CustomerId, EmployeeId, OrderDate, RequiredDate, ShippedDate, ShipVia, Freight, ShipName, ShipAddress, ShipCity, ShipRegion, ShipPostalCode, ShipCountry)
            VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)"""
    cur.executemany(query, all_values)

    ## Query the databse
    data2 = cur.execute("""
        SELECT count(Quantity) 
        FROM OrderDetail
        JOIN output_orders_csv
            ON output_orders_csv.Id = OrderDetail.OrderId 
            WHERE ShipCity = 'Rio de Janeiro';
    """)

    ## Write a file called "count.txt"
    with open("count.txt", "w") as f:
        for row in data2.fetchall():
            csv.writer(f).writerow(row)

    #print(type("count.txt"))

    ## Commit and colose connection 
    conn.commit()
    conn.close()
    return 

```



## DAG das tasks e declarando as dependências

Aqui está a a dag das duas tasks e as dependências.


``` 
    ## 1) Reads the data from the 'Order' table and writes a file called "output_orders.csv".
    task_1 = PythonOperator(
        task_id='task1',
        python_callable=task1,
        provide_context=True
    )

    ## 2) reads the data from the "OrderDetail" table, 
    ## makes a JOIN with the "output_orders.csv" file and 
    ## calculates the sum of the quantity sold (Quantity) destined (ShipCity) for Rio de Janeiro.
    ## export this count in "count.txt" file
    task_2 = PythonOperator(
        task_id='task2',
        python_callable=task2,
        provide_context=True
    )

```



## Resultados finais 

Os resultados do desafio do airflow foram enviados via Moodle.

- Arquivo final_output.txt 
- Link para o repositório do desafio


