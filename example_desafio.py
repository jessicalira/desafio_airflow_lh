from airflow.utils.edgemodifier import Label
from datetime import datetime, timedelta
from textwrap import dedent
from airflow.operators.bash import BashOperator
from airflow.operators.python import PythonOperator
from airflow import DAG
from airflow.models import Variable
import sqlite3
import csv


# These args will get passed on to each operator
# You can override them on a per-task basis during operator initialization
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email': ['airflow@example.com'],
    'email_on_failure': True,
    'email_on_retry': True,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}


## [Start function] 
def task1():
    ## Create a SQL connection to our SQLite database and a cursor
    conn = sqlite3.connect("data/Northwind_small.sqlite")
    ## Create a cursor
    cur = conn.cursor()

    ## Query the databse
    data1 = cur.execute('SELECT * FROM "Order"')

    ## Write a file called "output_orders.csv"
    with open("output_orders.csv", "w") as f:
        for row in data1.fetchall():
            csv.writer(f).writerow(row)

    ## Commit and colose connection 
    conn.commit()
    conn.close()
    return 

def task2():
    ## Import "output_orders.csv"
    ## Create a SQL connection to our SQLite database
    conn = sqlite3.connect("data/Northwind_small.sqlite")

    ## Create a cursor
    cur = conn.cursor()

    ## Drop table with same name
    cur.execute("Drop table if exists output_orders_csv;")
    
    ## Create table 
    cur.execute("""CREATE TABLE output_orders_csv (
        Id INTEGER, 
        CustomerId VARCHAR, 
        EmployeeId INTEGER,         
        OrderDate VARCHAR, 
        RequiredDate VARCHAR, 
        ShippedDate VARCHAR, 
        ShipVia INTEGER, 
        Freight DECIMAL, 
        ShipName VARCHAR, 
        ShipAddress VARCHAR, 
        ShipCity VARCHAR, 
        ShipRegion VARCHAR, 
        ShipPostalCode VARCHAR, 
        ShipCountry VARCHAR,
        PRIMARY KEY (Id)
        )""")
    
    ## Open "output_orders.csv" file
    with open("output_orders.csv",'r') as csvfile:
        csvfile = csv.reader(csvfile, delimiter=',')
        all_values = []
        for row in csvfile:
            value = (row[0], row[1],row[2],row[3], row[4],row[5],row[6], row[7],row[8], row[9],row[10],row[11], row[12],row[13])
            all_values.append(value)
    
    ## Insert values to table 
    query= """INSERT INTO output_orders_csv (Id, CustomerId, EmployeeId, OrderDate, RequiredDate, ShippedDate, ShipVia, Freight, ShipName, ShipAddress, ShipCity, ShipRegion, ShipPostalCode, ShipCountry)
            VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)"""
    cur.executemany(query, all_values)

    ## Query the databse
    data2 = cur.execute("""
        SELECT count(Quantity) 
        FROM OrderDetail
        JOIN output_orders_csv
            ON output_orders_csv.Id = OrderDetail.OrderId 
            WHERE ShipCity = 'Rio de Janeiro';
    """)

    ## Write a file called "count.txt"
    with open("count.txt", "w") as f:
        for row in data2.fetchall():
            csv.writer(f).writerow(row)

    #print(type("count.txt"))

    ## Commit and colose connection 
    conn.commit()
    conn.close()
    return 

## [End function]


## Do not change the code below this line ---------------------!!#
def export_final_answer():
    import base64

    # Import count
    with open('count.txt') as f:
        count = f.readlines()[0]

    my_email = Variable.get("my_email")
    message = my_email+count
    message_bytes = message.encode('ascii')
    base64_bytes = base64.b64encode(message_bytes)
    base64_message = base64_bytes.decode('ascii')

    with open("final_output.txt","w") as f:
        f.write(base64_message)
    return None
## Do not change the code above this line-----------------------##

with DAG(
    'DesafioAirflow',
    default_args=default_args,
    description='Desafio de Airflow da Indicium',
    schedule_interval=timedelta(days=1),
    start_date=datetime(2021, 1, 1),
    catchup=False,
    tags=['example'],
) as dag:
    dag.doc_md = """
        Esse é o desafio de Airflow da Indicium.
    """

    ## 1) Reads the data from the 'Order' table and writes a file called "output_orders.csv".
    task_1 = PythonOperator(
        task_id='task1',
        python_callable=task1,
        provide_context=True
    )

    ## 2) reads the data from the "OrderDetail" table, 
    ## makes a JOIN with the "output_orders.csv" file and 
    ## calculates the sum of the quantity sold (Quantity) destined (ShipCity) for Rio de Janeiro.
    ## export this count in "count.txt" file
    task_2 = PythonOperator(
        task_id='task2',
        python_callable=task2,
        provide_context=True
    )

    export_final_output = PythonOperator(
        task_id='export_final_output',
        python_callable=export_final_answer,
        provide_context=True
    )

task_1 >> task_2 >> export_final_output